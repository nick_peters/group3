#include <emc/io.h>
#include <opencv2/highgui/highgui.hpp>
#include <ros/rate.h>
#include <iostream>

#include "../src/communication.h"
#include "../src/motion.h"
#include "../src/pathfinding.h"
#include "../src/walldetection.h"
#include "../src/doorhandling.h"

#define PI 3.141592
using namespace std;

double resolution = 0.01;
cv::Point2d canvas_center;

// ----------------------------------------------------------------------------------------------------

cv::Point2d worldToCanvas(double x, double y)
{
    return cv::Point2d(-y / resolution, -x / resolution) + canvas_center;
}

// ----------------------------------------------------------------------------------------------------

CornerPoints getCornerPoints(emc::LaserData laser_data)
{
    CornerPoints points;

    //cout << "starting left" << endl;

    // left side
    int i = 500;
    double nearest_point = INFINITY;
    double current_point = INFINITY;
    float angle = 0;
    while (i < 1000)
    {
        // limit data and correct 0 reading for really far away
        if (laser_data.ranges[i] < 0.01 || laser_data.ranges[i] > INFINITY)
        {
            current_point = INFINITY;
        }
        else
        {
            current_point = laser_data.ranges[i];
        }

        if (current_point - nearest_point > 0.05 * nearest_point) // allready found nearest point;
            break;

        if (current_point < nearest_point)
        {
            nearest_point = current_point;
            angle = i;
            //cout << current_point << endl;
        }

        i++;
    }

    points.left = nearest_point;
    points.left_angle = -2 + angle * laser_data.angle_increment;

    //cout << "starting right" << endl;

    // right side
    i = 500;
    nearest_point = INFINITY;
    current_point = INFINITY;
    while (i > 1)
    {
        // limit data and correct 0 reading for really far away
        if (laser_data.ranges[i] < 0.01 || laser_data.ranges[i] > INFINITY)
        {
            current_point = INFINITY;
        }
        else
        {
            current_point = laser_data.ranges[i];
        }

        if (current_point - nearest_point > 0.05 * nearest_point) // allready found nearest point;
            break;

        if (current_point < nearest_point)
            nearest_point = current_point;
        angle = i;

        i--;
    }

    points.right = nearest_point;
    points.right_angle = -2 + angle * laser_data.angle_increment;
    //cout << "lp: " << points.left << " rp: " << points.right << " la: " 
    //<< points.left_angle << " ra: " << points.right_angle << endl;

    return points;
}

int main(int argc, char **argv)
{
    emc::IO io;

    // initialize supervisors from our code
    WallDetectionSupervisor wallSup;
    wallSup.setIO(&io);

    ros::Rate r(30);
    while (io.ok())
    {
        // define canvas to draw on
        cv::Mat canvas(500, 500, CV_8UC3, cv::Scalar(50, 50, 50));
        canvas_center = cv::Point2d(canvas.rows / 2, canvas.cols / 2);

        // colors are in BGR (Blue Green Red) because RGB was too mainstream
        cv::Scalar red(0, 0, 255);
        cv::Scalar lightgray(100, 100, 100);
        cv::Scalar green(0, 255, 0);
        cv::Scalar blue(255,0,0);
        cv::Scalar robot_color = red;
        cv::Scalar orange(0, 128, 255);

        // draw the pico
        std::vector<std::pair<double, double>> robot_points;
        robot_points.push_back(std::pair<double, double>(0.1, -0.2));
        robot_points.push_back(std::pair<double, double>(0.1, -0.1));
        robot_points.push_back(std::pair<double, double>(0.05, -0.1));
        robot_points.push_back(std::pair<double, double>(0.05, 0.1));
        robot_points.push_back(std::pair<double, double>(0.1, 0.1));
        robot_points.push_back(std::pair<double, double>(0.1, 0.2));
        robot_points.push_back(std::pair<double, double>(-0.1, 0.2));
        robot_points.push_back(std::pair<double, double>(-0.1, -0.2));

        for (unsigned int i = 0; i < robot_points.size(); ++i)
        {
            unsigned int j = (i + 1) % robot_points.size();
            cv::Point2d p1 = worldToCanvas(robot_points[i].first, robot_points[i].second);
            cv::Point2d p2 = worldToCanvas(robot_points[j].first, robot_points[j].second);
            cv::line(canvas, p1, p2, robot_color, 2);
        }

        // get laser data
        emc::LaserData scan;
        if (!io.readLaserData(scan))
            continue;

        

        //draw all the laser points
        double a = scan.angle_min;
        for (unsigned int i = 0; i < scan.ranges.size(); ++i)
        {
            double x = cos(a) * scan.ranges[i];
            double y = sin(a) * scan.ranges[i];

            cv::Point2d p = worldToCanvas(x, y);
            if (p.x >= 0 && p.y >= 0 && p.x < canvas.cols && p.y < canvas.rows)
                canvas.at<cv::Vec3b>(p) = cv::Vec3b(0, 255, 0);

            a += scan.angle_increment;
        }

        //drawe circle around pico
        //cv::Point2d p1 = worldToCanvas(robot_points[i].first, robot_points[i].second);
        cv::circle(canvas, canvas_center, 0.3 / resolution, orange, 2);

        // draw angle from origin
        emc::OdometryData odom;
        if (!io.readOdometryData(odom))
            continue;

        double compas_radius = 0.3 / resolution;
        cv::Point2d left_top_corner(compas_radius + 10, compas_radius + 10);
        cv::circle(canvas, left_top_corner, compas_radius, lightgray, 2);
        double compas_x = sin(PI - odom.a) * compas_radius;
        double compas_y = cos(PI - odom.a) * compas_radius;
        cv::Point2d compas_pos(compas_x, compas_y);
        cv::line(canvas, left_top_corner, compas_pos + left_top_corner, lightgray, 2);

        //Do stuff from our code
        CornerPoints points;
        points = getCornerPoints(scan);

        cv::Point2d left_corner((points.left * sin(points.left_angle - PI)) / resolution,
                                (points.left * cos(points.left_angle - PI)) / resolution);
        cv::Point2d right_corner((points.right * sin(points.right_angle - PI)) / resolution,
                                 (points.right * cos(points.right_angle - PI)) / resolution);
        cv::circle(canvas, canvas_center + left_corner, 3, blue, 2);
        cv::circle(canvas, canvas_center + right_corner, 3, green, 2);
        cv::imshow("PICO Viz", canvas);
        cv::waitKey(3);

        r.sleep();
    }

    return 0;
}