#include "worldinterpreting.h"
#include <emc/io.h>
#include <cmath>
#include <iostream>
#include <vector>
#include <numeric>

using namespace std;

// the function that gets called every loop of the main program
void WorldInterpreter::runWorldInterpreterLoop(DoorHandlingStatus door_status)
{
    loop_counter++;
    forwardVirtualArc(); // check for obstacles in frond when in the "going forward" pledge state
    //noWrapAngle();       // use some athimetric to calculate the total angle turned since strat without wrapping from -pi to +pi
    checkAlignment();   // When following the wall, check if pico is aligned to the nearest point
    checkWallInFront(); // check if pico is nearing an obstacle in front (so PICO can slow down and take a carefull turn)
    findNearestPoint(); // Find the nearest point for the virtual circle
    deadEndDetection(); // Check if PICO is in a dead end (possible door)

    odom_mem = moveDetection(odom_mem, door_status);

    //cout << "[worldSup/runWorldInterpreterLoop] angle increment: " << laser_data.angle_increment << endl;
}

// take a slice of the total laser range data and calculate the average (mean) value
double WorldInterpreter::laserAverage(int start, int end)
{
    if (start > end)
    {
        cerr << "[worldSup/laserAverage] Error: starting index i= " << start << " is larger than ending index i=" << end << endl;
    }

    if (start < 0 || end > 999)
    {
        cerr << "[worldSup/laserAverage] Error: starting index i= " << start << " or ending index i= " << end << " out of bounds." << endl;
    }

    float cumulative_sum = 0;
    for (int i = start; i <= end; i++)
    {
        cumulative_sum += laser_data.ranges[i];
    }

    return cumulative_sum / (end - start);
}

// take a slice of the total laser range data and calculate the disctance of the closest point in that slice
double WorldInterpreter::laserMinimum(int start, int end)
{
    if (start > end)
    {
        cerr << "[worldSup/laserAverage] Error: starting index i= " << start << " is larger than ending index i=" << end << endl;
    }

    if (start < 0 || end > 999)
    {
        cerr << "[worldSup/laserAverage] Error: starting index i= " << start << " or ending index i= " << end << " out of bounds." << endl;
    }

    float min_value = INFINITY_TRESHHOLD;
    for (int i = start; i <= end; i++)
    {
        if (laser_data.ranges[i] < min_value)
            min_value = laser_data.ranges[i];
    }

    return min_value;
}

// this function is used to determine when to get out of the
// going forward state in the pledge, it's like the virtual circle
// but only directly in front of the pico.
//
// I also know the variable names are silly, but since im in a hurry
// and we allready have simmilar function that use the sensible names
// this will have to do for now
void WorldInterpreter::forwardVirtualArc()
{
    pledge_forward_clear = false;
    float nearest_front_point_distance = laserMinimum(304, 696);
    if (nearest_front_point_distance > 0.5)
        pledge_forward_clear = true;
}

void WorldInterpreter::noWrapAngle()
{
    float angle_diff = odometry_data.a - previous_angle;

    if (angle_diff < -PI)
    {
        offset_angle_count += 1;
    }
    else if (angle_diff > PI)
    {
        offset_angle_count -= 1;
    }

    no_wrap_angle = odometry_data.a + 2 * PI * offset_angle_count;
    previous_angle = odometry_data.a;
    //cout << no_wrap_angle << endl;
}

// See if pico is aligned to the right wall
void WorldInterpreter::checkAlignment()
{
    float max_angular_deviation = 0.3;
    aligned_to_wall = abs(nearest_point_angle + 0.1 * PI) < max_angular_deviation;
}

// detect if PICO is currently facing a dead end (possible door)
bool WorldInterpreter::deadEndDetection()
{
    float frontmean = laserAverage(490, 510);
    float rightmean = laserAverage(107, 127);
    float leftmean = laserAverage(773, 793);

    if (leftmean < 1.1 && rightmean < 0.5 && frontmean < 0.5 && aligned_to_wall) // IMPORTANT dependent on virtual cirlcle! radius MUST be bigger that outer circle
    {
        facing_dead_end = true;
    }
    else
    {
        facing_dead_end = false;
    }
}

// this value is used in motion supervisor to slow the pico down when nearing an obstacle
bool WorldInterpreter::checkWallInFront()
{
    float frontmean = laserAverage(490, 510);
    if (frontmean < 0.9)
    {
        wall_in_front = true;
    }
    else
    {
        wall_in_front = false;
    }
}

// modify laser data in place to make sure all values are within bounds
// laser data from very far gets registered as valuas around 0, so we set
// those to  INFINITY_TRESHHOLD as well. basically we clip data between 0.01 and INFINITY_TRESHHOLD
void WorldInterpreter::parseLaserData()
{
    for (int i = 0; i < 1000; i++)
    {
        //cout << ", " << laser_data.ranges[i];
        if (laser_data.ranges[i] < 0.01 || laser_data.ranges[i] > INFINITY_TRESHHOLD)
            laser_data.ranges[i] = INFINITY_TRESHHOLD;
    }
    //cout << "\n\n\n\n\n\n\n\n\n" ;
}

// from all datapoints of the LRF find the one closest to pico and find it's distance and angle
void WorldInterpreter::findNearestPoint()
{
    float smallest_value = INFINITY_TRESHHOLD;
    int index = 0;
    int max_index = angleToIndex(0.1 * PI);

    //cout << "[WorldInterpreter/findNearestPoint] max_index = " << max_index << endl;

    for (int i = 0; i < max_index; i++)
    {
        if (laser_data.ranges[i] < smallest_value)
        {
            smallest_value = laser_data.ranges[i];
            index = i;
        }
    }

    nearest_point_distance = smallest_value;
    nearest_point_angle = indexToAngle(index);
}

// take an index value from 0 to 1000 from the LRF data and return the corresponding angle in radians
float WorldInterpreter::indexToAngle(int index)
{
    return -2 + index * laser_data.angle_increment;
}

// do the opposite of indexToAngle and find the corresponding index to a radian value between [-2,2]
int WorldInterpreter::angleToIndex(float angle)
{
    int index = round((angle + 2) / laser_data.angle_increment);

    if (index < 0 || index > 1000)
    {
        cerr << "[WorldSup/angleToIndex]: Error calculated index out of bounds, i:" << index << endl;
        // index = 0; //so the program doesnt crash.
    }

    return index;
}

// return all calculated information in the form of the worldmodel to the main function
WorldModel WorldInterpreter::getWorldModel()
{
    WorldModel world_model;

    world_model.wall_in_front = wall_in_front;
    world_model.nearest_point_angle = nearest_point_angle;
    world_model.nearest_point_distance = nearest_point_distance;
    world_model.facing_dead_end = facing_dead_end;

    world_model.no_wrap_angle = no_wrap_angle;
    world_model.pledge_forward_clear = pledge_forward_clear;

    world_model.turning = turning;
    return world_model;
}

// recieve all sensor information from the main loop and put it in internal (private) class variables
void WorldInterpreter::setSensorData(emc::LaserData laser_data_arg, emc::OdometryData odometry_data_arg)
{
    laser_data = laser_data_arg;
    odometry_data = odometry_data_arg;
    parseLaserData();
}

// count the number of turns taken by pico for the pledge algorithm
OdomMem WorldInterpreter::moveDetection(OdomMem odom_mem, DoorHandlingStatus door_status)
{
    if (door_status.waiting_for_door_to_open || door_status.ring_doorbell)
    {
    }
    else
    {
        if (odom_mem.timer == 10)
        {
            // Step memory
            odom_mem.fifth = odom_mem.fourth;
            odom_mem.fourth = odom_mem.third;
            odom_mem.third = odom_mem.second;
            odom_mem.second = odom_mem.first;

            // Slipping Odometry
            float data = odometry_data.a;

            // Overflow correction
            if (odom_mem.first - data > PI)
                data += 2 * PI;

            if (data - odom_mem.first > PI)
                data -= 2 * PI;

            // Calculate the Change since last sample
            float cor = data - odom_mem.correction;

            // Distiguise Normal PID action from turning
            // First put new value to zero, as if it is PID turning
            odom_mem.first = 0;

            // If turning is more than just PID action, correct
            // One overflow is considered possible because of sensor errors

            if (cor > 0.35 || cor < -0.35)
            {
                if (cor < 5.5 && cor > -5.5)
                    odom_mem.first = cor;
            }
            // Memory for change calculation next sample
            odom_mem.correction = data;
            cout << odom_mem.first << " " << odom_mem.second << " " << odom_mem.third << " " << odom_mem.fourth << " " << odom_mem.fifth << " " << endl;
            // Reset timer
            odom_mem.timer = -1;

            // Turn detection

            // Detect turn if new value is already PID action
            // But in the old values is still a turn visible
            // Conclusion, turn is finished
            // Assuming every 90 degrees angle is followed by a small amount of PID wall following before new corner.

            if (std::abs(odom_mem.first) < 0.25)
            {
                if (checkTurn(false, odom_mem))
                {
                    // Turning left is positive
                    turning++;

                    // 180 degree corrections
                    if (secondTurn(odom_mem, odometry_data.a, 1))
                        turning++;

                    // Reset Reference Angle after every turn
                    odom_mem.ref_angle = odometry_data.a;

                    //Reset memomry
                    odom_mem = resetMem(odom_mem);
                }
                if (checkTurn(true, odom_mem))
                {
                    // Turning right is negative
                    --turning;

                    // 180 degree corrections
                    if (secondTurn(odom_mem, odometry_data.a, -1))
                        --turning;

                    // Reset Reference Angle after every turn
                    odom_mem.ref_angle = odometry_data.a;

                    //Reset memomry
                    odom_mem = resetMem(odom_mem);
                }
            }
        }
        odom_mem.timer++;
        cout << "[WorldInterpreter/moveDetection] Turn counter (Left is positive): " << turning << endl;
    }
    return odom_mem;
}

bool WorldInterpreter::checkTurn(bool check, OdomMem odom_mem)
{
    // Check if Change is big enough to count as turn
    // Because of faulty Odometry Data, we assume one overflow is possible.
    // Therefore we check values around 0, below 2*PI and above -2*PI

    int x, y;
    if (check)
    {
        // Either lower than 0.3 or higer than 4 (overflow) for turn
        x = -0.3;
        y = 4;
    }
    else
    {
        // Either higher than 0.3 or lower than -4 (overflow) for turn
        x = -4;
        y = 0.3;
    }

    // If on of the last three samples gives value -> turn
    if (odom_mem.third < x || odom_mem.fourth < x || odom_mem.fifth < x)
        return true;

    if (odom_mem.third > y || odom_mem.fourth > y || odom_mem.fifth > y)
        return true;

    return false;
}
OdomMem WorldInterpreter::resetMem(OdomMem odom_mem)
{

    // Return all remembered changes to zero.
    odom_mem.fifth = 0;
    odom_mem.fourth = 0;
    odom_mem.third = 0;
    odom_mem.second = 0;
    return odom_mem;
}
bool WorldInterpreter::secondTurn(OdomMem odom_mem, float angle, int direction)
{
    // Find 180 degrees turn.
    // Compare angle before turn with angle after turn

    if (direction * (angle - odom_mem.ref_angle) < 0)
    {
        // Overflow correction
        angle = angle + direction * 2 * PI;
    }
    // If difference is far more than 90 degrees (0.5PI)
    // A 180 degree turn occured

    if (abs(angle - odom_mem.ref_angle) > 0.75 * PI)
        return true;

    return false;
}
