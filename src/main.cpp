#include <emc/io.h>
#include <emc/rate.h>
#include <iostream>

#include "communication.h"
#include "motion.h"
#include "pathfinding.h"
#include "worldinterpreting.h"
#include "doorhandling.h"

using namespace std;

int main()
{
    // Create IO object, which will initialize the io layer
    emc::IO io;

    // Create Rate object, which will help using keeping the loop at a fixed frequency
    emc::Rate rate(20);

    //Initiate Supervisors
    PathfindingSupervisor pathSup;
    MotionSupervisor motionSup;
    DoorHandlingSupervisor doorSup;
    WorldInterpreter worldSup;

    //Initialize communication Structs and variables
    WorldModel world_model;
    VelocitySetpoints velocities;
    DoorHandlingStatus door_handling_status;
    PledgeState pledge_state;

    //structs for laser and odometry data
    emc::LaserData laser_data;
    emc::OdometryData odometry_data;

    // Loop while we are properly connected
    while (io.ok())
    {
        //clear terminal output;
        //std::system("clear");

        // Get all information and skip loop if data is not available
        if (!io.readLaserData(laser_data) || !io.readOdometryData(odometry_data))
        {
            cerr << "[main/while] No new laser or odometry data available" << endl;
            rate.sleep();
            continue;
        }

        //run all the loops
        worldSup.setSensorData(laser_data, odometry_data);      // Give the LRF and odometry data to the world-interpreter
        worldSup.runWorldInterpreterLoop(door_handling_status); // Process the data into a world model
        world_model = worldSup.getWorldModel();                 // Get the world model

        pathSup.setWorldModel(world_model);      // Give the world model to the pathfinding algorithm
        pathSup.runPathfindingLoop();            // Run the pledge algorithm (determine if PICO should move straight ahead or follow the wall)
        pledge_state = pathSup.getPledgeState(); // Get the information about abovementioned decision

        doorSup.setWorldModel(world_model);                     // Give the world model to the Doorhandling algorithm
        doorSup.runDoorHandlingLoop();                          // Check (or activate) the timer if facing dead end
        door_handling_status = doorSup.getDoorHandlingStatus(); // Get waiting status

        motionSup.setWorldModel(world_model);
        motionSup.setDoorHandling(door_handling_status); // give the waiting status of the doorhandling to the motion algorithm
        motionSup.setPledgeState(pledge_state);          // Tell the motion algorithm to follow the wall or drive straight
        motionSup.runMotionLoop();                       // Run the motion algorithm
        velocities = motionSup.getVelocities();          // Get the velocity setpoints for the actuators

        // The final act for the loop is to set the speed setpoints of the actuators
        // give the final actuation commands to pico
        io.sendBaseReference(velocities.x_velocity, velocities.y_velocity, velocities.angular_velocity);

        if (door_handling_status.ring_doorbell)
            io.sendOpendoorRequest();

        // Sleep remaining time
        rate.sleep();
    }

    return 0;
}
