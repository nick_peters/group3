#include "motion.h"
#include "communication.h"
#include "pathfinding.h"
#include <iostream>
#include <cmath>

using namespace std;

void PathfindingSupervisor::runPathfindingLoop()
{
    following_wall = true;

    // Nick's code for Angle calculation
    // if(abs(no_wrap_angle) < 0.2 && pledge_forward_clear)
    //     following_wall = false;

    // Jelte's code for Angle calculation
    if (turning == 0 && pledge_forward_clear)
        following_wall = false;
}

PledgeState PathfindingSupervisor::getPledgeState()
{
    PledgeState pledge_state;

    pledge_state.following_wall = following_wall;

    return pledge_state;
}

// take all the relevant information from the world model and put it in internal variables
// it is important to do this for all needed data that comes from an external source
// so the code is "decoupled" and easy to modify
void PathfindingSupervisor::setWorldModel(WorldModel world_model)
{
    no_wrap_angle = world_model.no_wrap_angle;
    pledge_forward_clear = world_model.pledge_forward_clear;
    turning = world_model.turning;
}
