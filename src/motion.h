#ifndef MOTION
#define MOTION

#include <emc/io.h>
#include "communication.h"
#include "pid.h"

class MotionSupervisor
{
  private:
	WorldModel world_model;
	Direction direction;
	DoorHandlingStatus door_handling_status;

	bool following_wall = false;

	float driving_speed = 0;
	float angular_velocity = 0;
	float x_velocity = 0;
	float y_velocity = 0;

	float current_angle = 0.0;
	float desired_angle = 0.0;
	double pid_process_value = 0;
	double pid_reference_value = 0;
	double pid_output = 0;
	double pid_output_r = 0;
	double pid_output_theta = 0;

	//PID pid = PID(dt, max, min, kp, kd, ki )
	PID pid_r = PID(0.05, 1, -1, 5, 0.0, 0.3);
	PID pid_theta = PID(0.05, 1, -1, 5, 0.0, 0.3);

	bool wall_in_front = false;
	bool waiting_for_door_to_open = false;

	float nearest_point_distance = 0.0f;
	float nearest_point_angle = 0.0f;

	void stickToWall();
	void pidControl();
	void driveForward();

  public:
	void runMotionLoop();
	void setWorldModel(WorldModel world_model);
	VelocitySetpoints getVelocities();
	bool setDoorHandling(DoorHandlingStatus door_handling_status);
	void setPledgeState(PledgeState pledge_state);
};

#endif // MOTIONSUPERVISOR
