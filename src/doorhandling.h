#ifndef DOORHANDLE
#define DOORHANDLE

#include <emc/io.h>
#include "communication.h"

class DoorHandlingSupervisor
{
private:
  WorldModel world_model;

  int loop_counter = 0;
  bool waiting_for_door_to_open = false;
  bool doorhandling_active = false;

  bool ring_doorbell = false;
  bool facing_dead_end = false;
  int reset_countdown = 0;

public:
  void runDoorHandlingLoop();
  void activateDoorHandling();
  void setWorldModel(WorldModel world_model);
  DoorHandlingStatus getDoorHandlingStatus();
};

#endif
