#ifndef WORLDINT
#define WORLDINT

#include <emc/io.h>
#include "communication.h"

#define INFINITY_TRESHHOLD 2
#define PI 3.14159265359

struct OdomMem
{
  float first = 0.0f;
  float second = 0.0f;
  float third = 0.0f;
  float fourth = 0.0f;
  float fifth = 0.0f;
  float correction = 0.0f;
  float ref_angle = 0.0f;
  int timer = 0;
};

class WorldInterpreter
{
private:
  float nearest_point_distance = 0.0f;
  float nearest_point_angle = 0.0f;

  bool facing_dead_end = false;
  bool wall_in_front = true;
  bool aligned_to_wall = false;
  int turn_count = 0;
  int loop_counter = 0;

  emc::LaserData laser_data;
  emc::OdometryData odometry_data;

  // turn detection
  float previous_angle = 0;
  int offset_angle_count = 0;
  float no_wrap_angle = 0;
  int turning = 0;
  //float ref_angle =0.0f;
  OdomMem odom_mem;
  bool pledge_forward_clear = false;

  double laserAverage(int start, int end);
  void parseLaserData();
  void findNearestPoint();
  bool checkWallInFront();
  float indexToAngle(int index);
  int angleToIndex(float angle);
  bool deadEndDetection();
  void checkAlignment();
  void forwardVirtualArc();
  double laserMinimum(int start, int end);
  void noWrapAngle();
  bool checkTurn(bool check, OdomMem odom_mem);
  OdomMem resetMem(OdomMem odom_mem);
  bool secondTurn(OdomMem odom_mem, float angle, int direction);

public:
  void setSensorData(emc::LaserData laser_data_arg, emc::OdometryData odometry_data_arg);
  void runWorldInterpreterLoop(DoorHandlingStatus door_handling_status);
  OdomMem moveDetection(OdomMem odom_mem, DoorHandlingStatus door_status);
  WorldModel getWorldModel();
};

#endif
