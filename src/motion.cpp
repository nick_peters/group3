#include <emc/io.h>
#include <emc/rate.h>
#include <iostream>
#include <cmath>

#include "communication.h"
#include "motion.h"
#include "pid.h"

#define TAU 6.2831
#define PI 3.1416
using namespace std;

void MotionSupervisor::runMotionLoop()
{
    if (waiting_for_door_to_open) //wait until door opens
    {
        // Set all velocities to 0
        x_velocity = 0;
        y_velocity = 0;
        angular_velocity = 0;
        cout << "[MotionSupervisor/runMotionLoop] waiting for door to open" << endl;
        return;
    }

    if (following_wall)
    {
        stickToWall();
    }
    else
    {
        driveForward();
    }
}

void MotionSupervisor::driveForward()
{
    float driving_speed = 0.5;
    x_velocity = driving_speed;
    y_velocity = 0;
    angular_velocity = 0;
}

void MotionSupervisor::stickToWall()
{
    float driving_speed = 0.5;

    if (wall_in_front)
        driving_speed = 0.2;

    // define the size of the virtual circle that surrounds pico
    // pico will try to keep itself parralel to the wall by putting the nearest
    // point to the right of itself
    float virtual_circle_radius = 0.4;
    float follow_radius = 0.3;

    // if the closest point is near the right side of pico it can move forward while
    // alligning, if not it aligns first
    float max_angular_deviation = 0.3;

    // a wall is touching the virtual circle
    if (nearest_point_distance < virtual_circle_radius)
    {
        // PID for theta
        pid_process_value = nearest_point_angle;
        // 0.45 is slightly offset to the right so pico doesn't turn awayfrom the wall
        pid_reference_value = -0.45 * PI;
        pid_output_theta = -pid_theta.calculate(pid_reference_value, pid_process_value);

        // angular error is to big, align first
        if (abs(nearest_point_angle + 0.45 * PI) > max_angular_deviation)
        {
            angular_velocity = pid_output_theta;
            x_velocity = 0;
            y_velocity = 0;
        }
        else //angular alignment is fine, use r PID as well
        {
            // PID for r
            pid_process_value = nearest_point_distance;
            pid_reference_value = follow_radius;
            pid_output_r = pid_r.calculate(pid_reference_value, pid_process_value);

            // combing r and theta OID output
            angular_velocity = pid_output_theta;
            x_velocity = driving_speed;
            y_velocity = pid_output_r;
        }
    }
    else // no wall is touching the virtual circle
    {
        x_velocity = driving_speed;
        y_velocity = 0;
        angular_velocity = 0;
    }
}

// take all the relevant information from the world model and put it in internal variables
// it is important to do this for all needed data that comes from an external source
// so the code is "decoupled" and easy to modify
void MotionSupervisor::setWorldModel(WorldModel world_model)
{
    wall_in_front = world_model.wall_in_front;
    nearest_point_distance = world_model.nearest_point_distance;
    nearest_point_angle = world_model.nearest_point_angle;
}

// the velocity is set at the end of the main loop, so the safety supervisor has a chance to override the values
VelocitySetpoints MotionSupervisor::getVelocities()
{
    VelocitySetpoints velocities;
    velocities.x_velocity = x_velocity;
    velocities.y_velocity = y_velocity;
    velocities.angular_velocity = angular_velocity;

    return velocities;
}

bool MotionSupervisor::setDoorHandling(DoorHandlingStatus door_handling_status)
{
    waiting_for_door_to_open = door_handling_status.waiting_for_door_to_open; //setting the value from door handling
}

void MotionSupervisor::setPledgeState(PledgeState pledge_state)
{
    following_wall = pledge_state.following_wall;
}
