#ifndef COMMUNICATION
#define COMMUNICATION

struct WorldModel
{

    bool wall_in_front = false;
    bool facing_dead_end = false;
    bool pledge_forward_clear = false;

    float nearest_point_distance = 0.0f;
    float nearest_point_angle = 0.0f;
    float no_wrap_angle = 0;
    int turning = 0;
};

struct VelocitySetpoints
{
    float x_velocity = 0.0f;
    float y_velocity = 0.0f;
    float angular_velocity = 0.0f;
};

struct PledgeState
{
    bool following_wall = false;
    int turn_count = 0;
};

enum class Direction
{
    front,
    right,
    left,
    back,
};

struct DoorHandlingStatus
{
    bool waiting_for_door_to_open = false;
    bool ring_doorbell = false;
};

#endif
