#include "communication.h"
#include "doorhandling.h"
#include <cmath>
#include <iostream>

using namespace std;

void DoorHandlingSupervisor::runDoorHandlingLoop()
{
    ring_doorbell = false;

    if (reset_countdown > 0)
        reset_countdown--;

    //cout << "[Doorhandling/runDoorHandlingLoop]" << endl;
    //cout << "reset countdown: " << reset_countdown << endl;
    //cout << "loop counter: " << loop_counter << endl;

    if (!waiting_for_door_to_open && facing_dead_end && reset_countdown <= 0)
    {
        activateDoorHandling();
    }

    // increment timer to wait 5 sec.
    if (waiting_for_door_to_open)
    {
        loop_counter++;
    }

    if (loop_counter >= 100) //100 cycles = 5 sec. hardcoded for now to run smoothly in simulation
    {
        waiting_for_door_to_open = false;
        reset_countdown = 50; // when reset counter is above 0 the doorhandling will not activate to avoid getting stuck in dead ends
        loop_counter = 0;
    }
}

void DoorHandlingSupervisor::activateDoorHandling()
{
    waiting_for_door_to_open = true;
    ring_doorbell = true;
    loop_counter = 0; // IMPORTANT: Loop counter expects a loop frequency of ~20 Hz
    cout << "[Doorhandling/activate] Doorhandling supervisor activated, attempting to open door!" << endl;
}

// returns true when active (waiting for door to dissapear)
DoorHandlingStatus DoorHandlingSupervisor::getDoorHandlingStatus()
{
    DoorHandlingStatus door_handling_status;
    door_handling_status.ring_doorbell = ring_doorbell;
    door_handling_status.waiting_for_door_to_open = waiting_for_door_to_open;
    return door_handling_status; //to be used in motion loop
}

void DoorHandlingSupervisor::setWorldModel(WorldModel world_model)
{
    facing_dead_end = world_model.facing_dead_end;
}
