#ifndef PATHFINDING
#define PATHFINDING

#include "motion.h"

class PathfindingSupervisor
{
private:
  bool active = true;
  bool pledge_forward_clear = false;

  bool following_wall = false;
  float no_wrap_angle = 0;
  int turning = 0;

public:
  void setWorldModel(WorldModel world_model);
  void runPathfindingLoop();
  PledgeState getPledgeState();
};

#endif
