cmake_minimum_required(VERSION 2.8)
project(my_project)

# find_package(OpenCV REQUIRED)

# find_package(catkin REQUIRED COMPONENTS
#     geometry_msgs
#     sensor_msgs
#     roscpp
# )



# include_directories(
#     ${catkin_INCLUDE_DIRS}
#     ${OpenCV_INCLUDE_DIRS}
# )


add_executable(picorun 
                src/main.cpp
                src/pathfinding.cpp
                src/motion.cpp
                src/communication.cpp
                src/worldinterpreting.cpp
                src/doorhandling.cpp
                src/pid.cpp
                )


# add_executable(picoviz 
#                 visualization/visual.cpp
#                 src/pathfinding.cpp
#                 src/motion.cpp
#                 src/communication.cpp
                
#                 src/doorhandling.cpp
#                 src/pid.cpp)

# target_link_libraries(picoviz emc-framework ${catkin_LIBRARIES} ${OpenCV_LIBRARIES})

target_link_libraries(picorun emc-framework)
set(EXECUTABLE_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/bin)
set (CMAKE_CXX_FLAGS "--std=gnu++11 ${CMAKE_CXX_FLAGS}")
